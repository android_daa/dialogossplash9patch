package cl.duoc.dialogosdrawpatch;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.alertdialogpro.AlertDialogPro;

public class DialogExternosActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dialog_externos);
    }

    public void onClickDialogExterno(View v){
        AlertDialogPro.Builder builder = new AlertDialogPro.Builder(this);
        builder.setIcon(R.mipmap.ic_launcher).
                setTitle("Title").
                setMessage("Message Body").
                setNeutralButton("Neutral", null).
                setPositiveButton("Positive", null).
                setNegativeButton("Negative", null).
                show();
    }
}
